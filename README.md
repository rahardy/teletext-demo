<!--
SPDX-FileCopyrightText: © 2021 Tech and Software Ltd.
SPDX-License-Identifier: AGPL-3.0-only OR LicenseRef-uk.ltd.TechAndSoftware-1.0
-->

Demonstration page for using [`@techandsoftware/teletext`](https://www.npmjs.com/package/@techandsoftware/teletext). This shows the module rendering teletext pages using vector graphics (SVG) in the browser. The module provides an API to draw on the screen.

To run:

```bash
npm install
npm start
```

This will build the app in the `public/dist` directory and serve `public`.

# Notes about the demo app

* Two fonts are supplied in the fonts directory. For your own app, you only need the Unscii font if you want to render the mosaic characters using the font instead of graphics. The default is to use graphics. The Bedstead font is supplied to demonstrate alternative fonts and isn't required.

* The demo uses ES6 module imports for the app, which works in most browsers but not Internet Explorer. If you need to support IE or older browsers you can use the UMD version of the @techandsoftware/teletext module, but you'll also need to transpile the code as it uses modern Ecmascript syntax.

# Licensing

This package is licensed with GNU Affero General Public License 3 (AGPL-3.0-only), or a commerical license (LicenseRef-uk.ltd.TechAndSoftware-1.0) if you have paid a licensing fee. See the LICENSES subdirectory for license text.

The fonts have their own licenses. See the *.license files in the public/fonts directory.

This package complies with [REUSE 3](https://reuse.software)
