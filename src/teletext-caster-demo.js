/*
 SPDX-FileCopyrightText: © 2021 Tech and Software Ltd.
 SPDX-License-Identifier: AGPL-3.0-only OR LicenseRef-uk.ltd.TechAndSoftware-1.0
*/

import { ttxcaster } from '@techandsoftware/teletext-caster';

ttxcaster.castStateChanged.attach(castStateChanged);

function castStateChanged() {
    console.log('cast state is', ttxcaster.getCastState());
}

function initUI() {
    // the buttons map directly to API calls on ttxcaster
    document.querySelector('#revealButton').addEventListener('click', () => ttxcaster.toggleReveal());
    document.querySelector('#mixButton').addEventListener('click', () => ttxcaster.toggleMixMode());
    document.querySelector('#boxedButton').addEventListener('click', () => ttxcaster.toggleBoxMode());
    document.querySelector('#clearButton').addEventListener('click', () => ttxcaster.clearScreen());
    document.querySelector('#gridButton').addEventListener('click', () => ttxcaster.toggleGrid());
    document.querySelector('#smoothButton').addEventListener('click', () => ttxcaster.setSmoothMosaics());

    // the page data is embedded in the HTML. Here we'll get it using dataset and pass it to ttxcaster.display()
    document.querySelectorAll('[data-page]').forEach(e => e.addEventListener('click', () => ttxcaster.display(e.dataset.page)));
}

initUI();
